$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "wkm_bcms_publishing/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "wkm_bcms_publishing"
  s.version     = WkmBcmsPublishing::VERSION
  s.authors     = ["Pedro de Jesus"]
  s.email       = ["pjesus@wkm.com.br"]
  s.homepage    = "http://workingminds.com.br"
  s.summary     = "Plugin de publicacao para o BrowserCMS"
  s.description = "Plugin de publicacao para o BrowserCMS"

  s.files = Dir["{app,config,db,lib}/**/*"] + ["COPYRIGHT.txt", "Rakefile", "README.rdoc"]
  s.files -= Dir['lib/tasks/module_tasks.rake']
  s.test_files = Dir["test/**/*"]

  # Depend on BrowserCMS,rather than Rails 
  # s.add_dependency "rails", "~> 3.2.8"
  # s.add_dependency "jquery-rails"
  s.add_dependency "browsercms", "~> 3.5.0"
  s.add_dependency "sunspot_solr"
  s.add_dependency "sunspot_rails"
  s.add_dependency "workflow"

  #s.add_dependency "wkm_bcms_namespaces --local ../wkm_bcms_namespaces"
end
