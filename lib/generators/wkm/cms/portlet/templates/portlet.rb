class <%= class_name %>Portlet < Cms::Portlet
  include WkmBcmsNamespaces::NamespacePortlet

  # Uncomment this method to make this namespace portlet have a page route
  #have_route_with_params "(/:id)"

  def render
    # Your Code Goes Here
  end
    
end