require 'browsercms'


module WkmBcmsPublishing
  class Engine < ::Rails::Engine
    isolate_namespace WkmBcmsPublishing
		include Cms::Module
  end
end
